import data from '~/static/json/bikes.json'

export const state = () => ({
  bikes: [],
  bikeHeirarchy: [],
})

export const getters = {
  getBikes(state) {
    return state.bikes
  },
  getBikeHeirarchy(state) {
    return state.bikeHeirarchy
  },
}

export const mutations = {
  setBikes(state, payload) {
    state.bikes = payload
  },
  setBikeHeirarchy(state, payload) {
    state.bikeHeirarchy = payload
  },
}

export const actions = {
  async loadBikes({ dispatch }) {
    const bikes = await dispatch('transformBikes', data)
    await dispatch('loadBikeHeirarchy', bikes)
  },
  async transformBikes({ commit }, payload) {
    // Create slugs for links and images before saving
    payload.forEach((bike, index) => {
      let manufacturer = bike.manufacturer.toLowerCase()
      let model = bike.model.toLowerCase().replace(/\s+/g, '-')
      payload[index].slug = `/${manufacturer}/${model}/`
      payload[index].image = `/images/${model}.png`
    })
    commit('setBikes', payload)
    return payload
  },
  async loadBikeHeirarchy({ commit }, payload) {
    // Setup bike navigation structure
    let heirarchy = []
    // Map bike manufacturers to a new array and convert to a set to get a list of only uniques
    let manufacturers = [...new Set(payload.map((bike) => bike.manufacturer))]
    // Go through each unique manufacturer and build a map with the models for that manufacturer
    manufacturers.forEach((manufacturer) => {
      let bikes = payload.filter((bike) => bike.manufacturer == manufacturer)
      let models = bikes.map((bike) => {
        return {
          name: bike.model,
          url: bike.slug,
        }
      })

      heirarchy.push({
        manufacturer: manufacturer,
        models: models,
      })
    })
    await commit('setBikeHeirarchy', heirarchy)
  },
  async nuxtServerInit({ dispatch }) {
    await dispatch('loadBikes')
  },
}
