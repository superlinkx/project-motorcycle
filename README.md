# Project Motorcycle

## A Demo Project

Thanks for checking out this project. It was a speed build for an interview sample created in ~4 hours using only assets and information provided, and a simple color scheme/font guide. Example running at [https://pm.superlinkx.dev](). You can follow the instructions below to build the project.

## Docker

The docker setup for this project uses [global-docker](https://github.com/superlinkx/global-docker). Be sure to set it up first if using the default `docker-compose.yml`

You can start this project in dev mode by using `docker-compose up`. Dev url defaults to [http://project-motorcycle.localhost]()

You can also build and test a static build by using `docker-compose -f docker-compose-build.yml up`. The static build runs in an nginx container and can be accessed on [http://motorcycle-prod.localhost]()

## Nuxt Build Setup

Use this if you want to build with local node (non-Docker)

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn nuxt build && yarn nuxt export
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
