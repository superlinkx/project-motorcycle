FROM node:lts-buster-slim AS build

RUN apt-get -y update \
    && apt-get install -y git

RUN apt-get autoremove -y \
    && apt-get autoclean -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

COPY . /srv/project-motorcycle
# Fix file permissions
RUN chown -R node:node /srv/project-motorcycle
WORKDIR /srv/project-motorcycle
USER node
RUN yarn global add nuxt
RUN yarn install
RUN yarn nuxt build && yarn nuxt export

FROM nginx:latest
WORKDIR /usr/share/nginx/html
COPY --from=build /srv/project-motorcycle/dist /usr/share/nginx/html